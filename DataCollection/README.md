ascending_order.py : To convert normal movie list file to a new file with movie names in the descending order (movie name with maximum characters first)

			python3 ascending_order.py movies_list.txt movies_orderedlist.txt

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

movieReviews.py: This script writes all the reviews of a folder to an output file (with each file content from the folder to a new line in the output file)

			python3 movieReviews.py [FOLDER_PATH] movieReviews.txt

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

mergeReviewsAndNames.py: This script takes 3 arguments, 2 inputs and 1 output (output in format "movieName <====> movieReview"): 
1. movies_orderlist.txt 
2. movieReviews.txt
output: movieReviewsWithNames.txt

			python3 mergeReviewsAndNames.py movies_orderedlist.txt movieReviews.txt movieReviewsWithNames.txt 

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

appendReviewsInList.py : this script will merge all reviews of a certain movie into a dictionary entity with KEY as movie name and VALUE as list of reviews of the movie.

			python3 appendReviewsInList.py movieReviewsWithNames.txt appendedMovieReviews.txt


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

mergeReviewsAndCast.py : this script will merge all reviews of a certain movie into a dictionary entity with KEY as movie name and VALUE as list of reviews of the movie.

			python3 mergeReviewsAndCast.py movieCastInfo.txt appendedMovieReviews.txt processedMovieData.txt  


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sometimes in case of "one word movies" that common name appears in many reviews --> so remove that movie with common (frequent) name.

total movies : 17330



--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# One of the advantage is all the reviews are appropriately tokenized. So, every movie name would appear as a separate entity like "the bat" movie in the review text would appear as " the bat ". So, you can search the name of the movie in the text preceding and following with space.

# long movie names first followed by short movie names.

# lower case all the movie names and reviews to help detect movie names in the review.

# All the content (movie review) is properly tokenized. So, search for the whole word (movie name) in the review.

# For now, it will work better on movies with unique names.

# We are taking advantage of the fact that if the review compares the movie with some other movie and the other movie gets picked up by the script then also it is totally fine. Because, the reviewer is comparing 2 or more movies, it means all those movies shares some common characterstics described by the review. So, we can set that other movie with the given review.

# Remove all the spaces from the movie review and moviename. Create and store "MOVIENAMEis" and "MOVIENAME" and MOVIENAME

TO DO:
# Remove HTML tags from the reviews.
import sys;
import glob;
import os;
import re;

inputfile1 = sys.argv[1];
movieNamesFile = open(inputfile1, "r",errors='ignore');

inputfile2 = sys.argv[2];
movieReviewsFile = open(inputfile2, "r",errors='ignore');

outputfile = sys.argv[3];
movieNamewithReviewFile = open(outputfile, "w",errors='ignore');

movieNameList = list();
line1 = movieNamesFile.readline();
while(line1):
	line1 = line1.strip();
	line1 = line1.lower();
	movieNameList.append(line1);
	line1=movieNamesFile.readline();

print ("size of movieNameList is:", str(len(movieNameList)));
deletecounter = 1;
line2 = movieReviewsFile.readline();
while(line2):
	line2 = line2.strip();
	line3 = line2;
	line2 = line2.lower();
	line2 = " " + line2;
	reviewWithoutSpaces = line2.replace(" ","");
	#print ("original review is:", line2);
	#print ("review without space is:", reviewWithoutSpaces);
	reviewTagged = False;
	for item in movieNameList:
		movieNameWithoutSpaces = item.replace(" ","");
		movieNameCheck1 = "\"" +  movieNameWithoutSpaces +"\"" + "is";
		movieNameCheck2 = "\"" +  movieNameWithoutSpaces +"\"" + "stars";
		#movieNameCheck3 = "in" + "\"" +  movieNameWithoutSpaces +"\"";
		
		if ((movieNameCheck1 in reviewWithoutSpaces) or (movieNameCheck2 in reviewWithoutSpaces)):
			movieNamewithReviewFile.write(item + " <====> " + line3 + "\n");
			reviewTagged = True;
			break;

	if(not reviewTagged):
		for item in movieNameList:
			movieNameWithoutSpaces = item.replace(" ","");
			movieNameDoubleQoute1 = "\"" +  movieNameWithoutSpaces +"\"";
			movieNameSingleQoute1 = "'" +  movieNameWithoutSpaces + "'";

			if ((movieNameDoubleQoute1 in reviewWithoutSpaces) or (movieNameSingleQoute1 in reviewWithoutSpaces)):
				movieNamewithReviewFile.write(item + " <====> " + line3 + "\n");
				reviewTagged = True;
				break;

	if(not reviewTagged):
		for item in movieNameList:		
			movieNamePOS1 = item + " is"; 	
			movieNamePOS2 = item + " stars"; 	
			if ((movieNamePOS1 in line2) or (movieNamePOS2 in line2)):
				movieNamewithReviewFile.write(item + " <====> " + line3 + "\n");
				reviewTagged = True;
				break;
	
	if(not reviewTagged):
		for item in movieNameList:
			item2 = "\"" +  item +"\"";
			item = " " + item + " "; 				# Extra space is added to distinguish movie "the bat" from the movie "the batman". It will search for the whole word now.
			if ((item in line2) or (item2 in line2)):
				movieNamewithReviewFile.write(item + " <====> " + line3 + "\n");
				break;
	line2 = movieReviewsFile.readline();

movieNamesFile.close();
movieReviewsFile.close();
movieNamewithReviewFile.close();

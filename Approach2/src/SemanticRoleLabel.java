import com.googlecode.clearnlp.run.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.googlecode.clearnlp.component.AbstractComponent;
import com.googlecode.clearnlp.dependency.DEPTree;
import com.googlecode.clearnlp.engine.EngineGetter;
import com.googlecode.clearnlp.nlp.NLPDecode;
import com.googlecode.clearnlp.nlp.NLPLib;
import com.googlecode.clearnlp.reader.AbstractReader;
import com.googlecode.clearnlp.segmentation.AbstractSegmenter;
import com.googlecode.clearnlp.tokenization.AbstractTokenizer;
import com.googlecode.clearnlp.util.UTInput;
import com.googlecode.clearnlp.util.UTOutput;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class SemanticRoleLabel  {
int[] processedNodes;
HashMap<String,Double> posscore;
HashMap<String,Double> negscore;

public SemanticRoleLabel(String dictFile, String posModelFile, String depModelFile, String predModelFile, String roleModelFile, String srlModelFile, String inputFile, String outputFile) throws Exception
     {
             AbstractTokenizer tokenizer  = EngineGetter.getTokenizer(language, new FileInputStream(dictFile));
             AbstractComponent tagger     = EngineGetter.getComponent(new FileInputStream(posModelFile) , language, NLPLib.MODE_POS);
             AbstractComponent analyzer   = EngineGetter.getComponent(new FileInputStream(dictFile)     , language, NLPLib.MODE_MORPH);
             AbstractComponent parser     = EngineGetter.getComponent(new FileInputStream(depModelFile) , language, NLPLib.MODE_DEP);
             AbstractComponent identifier = EngineGetter.getComponent(new FileInputStream(predModelFile), language, NLPLib.MODE_PRED);
             AbstractComponent classifier = EngineGetter.getComponent(new FileInputStream(roleModelFile), language, NLPLib.MODE_ROLE);
             AbstractComponent labeler    = EngineGetter.getComponent(new FileInputStream(srlModelFile) , language, NLPLib.MODE_SRL);
             
             AbstractComponent[] components = {tagger, analyzer, parser, identifier, classifier, labeler};
             
             //Read File content
             File file=new File("C:\\Users\\sumeshm\\workspace\\SemanticRoleLabeling\\src\\review.txt");
             File outfile=new File("C:\\Users\\sumeshm\\workspace\\SemanticRoleLabeling\\src\\reviewout.txt");
             BufferedWriter writer = new BufferedWriter(new FileWriter(outfile));
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
               String line;
               while ((line = br.readLine()) != null) {
               	
               	JSONParser jsonParser = new JSONParser();
               	JSONObject obj=(JSONObject)jsonParser.parse(line); 
               	String cast1=(String) obj.get("cast1");
               	String cast2=(String) obj.get("cast2");
               	String cast3=(String) obj.get("cast3");
               	String cast4=(String) obj.get("cast4");
               	String originalName=(String) obj.get("originalName");
               	JSONArray reviews=(JSONArray)obj.get("reviewsDetail");
               	
               	String[] splitCast1 = cast1.split(" ");
           	String[] splitCast2 = cast2.split(" ");
           	String[] splitCast3 = cast3.split(" ");
           	String[] splitCast4 = cast4.split(" ");
           	
           	posscore=new HashMap<String,Double>();
           	negscore=new HashMap<String,Double>();
           	String reviewModified="";
           	boolean treeGenerated= false;
           	List<String> trees=new ArrayList<String>();
           	
               	for(int i=0;i<reviews.size();i++)
               	{
               	String review = (String) reviews.get(i);
               	reviewModified+=review.replace(cast1.toLowerCase(),cast1.toUpperCase()).replace(cast1,cast1.toUpperCase()).replace(cast2.toLowerCase(),cast2.toUpperCase()).replace(cast2,cast2.toUpperCase()).replace(cast3.toLowerCase(),cast3.toUpperCase()).replace(cast3,cast3.toUpperCase()).replace(cast4.toLowerCase(),cast4.toUpperCase()).replace(cast4,cast4.toUpperCase());
               	}
               	for(String cast : splitCast1)
               	{	
               	if(reviewModified.toLowerCase().contains(cast.toLowerCase()) && (treeGenerated== false) )
               	{
               	treeGenerated=true;
               	trees=process(tokenizer, components, reviewModified);
               	break;
               	}
               	
               	}
               	for(String cast : splitCast2)
               	{	
               	if(reviewModified.toLowerCase().contains(cast.toLowerCase()) && (treeGenerated== false) )
               	{
               	treeGenerated=true;
               	trees=process(tokenizer, components, reviewModified);
               	break;
               	}
               	
               	}
               	for(String cast : splitCast3)
               	{	
               	if(reviewModified.toLowerCase().contains(cast.toLowerCase()) && (treeGenerated== false) )
               	{
               	treeGenerated=true;
               	trees=process(tokenizer, components, reviewModified);
               	break;
               	}
               	
               	}
               	for(String cast : splitCast4)
               	{	
               	if(reviewModified.toLowerCase().contains(cast.toLowerCase()) && (treeGenerated== false) )
               	{
               	treeGenerated=true;
               	trees=process(tokenizer, components, reviewModified);
               	break;
               	}
               	
               	}
               	if(trees.size()!=0)
                   	ApplyRules(trees,cast1,cast2,cast3,cast4);
               	String result=originalName+"\t"+cast1+":"+posscore.get(cast1)+":"+negscore.get(cast1)+"\t"+cast2+":"+posscore.get(cast2)+":"+negscore.get(cast2)+"\t"+cast3+":"+posscore.get(cast3)+":"+negscore.get(cast3)+"\t"+cast4+":"+posscore.get(cast4)+":"+negscore.get(cast4)+"\n";
               	writer.write (result);
                   	
               	}
               	
               	
               }
            catch (Exception e) {
 	e.printStackTrace();
 	}
               writer.close();
     }
     
     
    private void ApplyRules(List<String> trees, String cast1, String cast2,String cast3, String cast4) 
    {
    String[] splitCast1 = cast1.split(" ");
String[] splitCast2 = cast2.split(" ");
String[] splitCast3 = cast3.split(" ");
String[] splitCast4 = cast4.split(" ");	
posscore.put(cast1,(double) 0);
posscore.put(cast2,(double) 0);
posscore.put(cast3,(double) 0);
posscore.put(cast4,(double) 0);
negscore.put(cast1,(double) 0);
negscore.put(cast2,(double) 0);
negscore.put(cast3,(double) 0);
negscore.put(cast4,(double) 0);
    for(String tree:trees)
    {
    List<Integer> nsubj=new ArrayList<Integer>();
    List<Integer> nsubjpass=new ArrayList<Integer>();
    List<Integer> acomp=new ArrayList<Integer>();
   
    Boolean cast1Mention=false;
    Boolean cast2Mention=false;
    Boolean cast3Mention=false;
    Boolean cast4Mention=false;
   
    String[] nodes=tree.split("\n");
   
    //Store the head nodes for each token
int[] headNodes=new int[nodes.length+1];
//Store the lemma nodes for each token
String[] lemmaNodes=new String[nodes.length+1];
//Store the POS tags for each token
String[] posNodes=new String[nodes.length+1];
//Store the token value for each token
String[] tokenName=new String[nodes.length+1];
processedNodes = new int[nodes.length+1];
    for(String node: nodes)
    {
    String[] component = node.split("\t");
    headNodes[0]=0;
    lemmaNodes[0]="";
    posNodes[0]="";
   
    headNodes[Integer.parseInt(component[0])]=Integer.parseInt(component[5]);
    lemmaNodes[Integer.parseInt(component[0])]=component[6];
    posNodes[Integer.parseInt(component[0])]=component[3];
    tokenName[Integer.parseInt(component[0])]=component[1];
   
    //Find nominal subject and nominal passive subject (clausal subjects are ignored)
    if(component[6].equals("nsubj"))
    nsubj.add(Integer.parseInt(component[0]));
    if(component[6].equals("nsubjpass"))
    nsubjpass.add(Integer.parseInt(component[0]));
    //Find adjective complement
    if(component[6].equals("acomp"))
    acomp.add(Integer.parseInt(component[0]));	
   
    }

    String prevNN="";
   
    for(int i=1;i<tokenName.length;i++)
    {
    for(String cast : splitCast1)
   	{
   	if(tokenName[i].toLowerCase().equals(cast.toLowerCase()))
   	{
   	cast1Mention=true;
   	int headId=headNodes[i];
   	String posTag=posNodes[i];
   	processedNodes[i]=1;
   	//Rule 1: Find the sentiments where actor's performance has been directly mentioned about
   	if((posNodes[headId]).contains("VB"))
   	{
   	processedNodes[headId]=1;
   	List<Integer> referredNodes=findHeads(headId,headNodes);
   	Collections.sort(referredNodes);
   	double[] polarityscore = computePolarity(referredNodes,tokenName,posNodes);
   	double currentscore=posscore.get(cast1);
   	posscore.put(cast1,currentscore+polarityscore[0]);
   	currentscore=negscore.get(cast1);
   	negscore.put(cast1,currentscore+polarityscore[1]);
   	}
   	
   	
   	}
   	}
   
   	for(String cast : splitCast2)
   	{
   	if(tokenName[i].toLowerCase().equals(cast.toLowerCase()))
   	{
   	cast2Mention=true;
   	int headId=headNodes[i];
   	String posTag=posNodes[i];
   	processedNodes[i]=1;
   	
   	if((posNodes[headId]).contains("VB"))
   	{
   	processedNodes[headId]=1;
   	List<Integer> referredNodes=findHeads(headId,headNodes);
   	Collections.sort(referredNodes);
   	double[] polarityscore = computePolarity(referredNodes,tokenName,posNodes);
   	double currentscore=posscore.get(cast2);
   	posscore.put(cast2,currentscore+polarityscore[0]);
   	currentscore=negscore.get(cast2);
   	negscore.put(cast2,currentscore+polarityscore[1]);
   	
   	}
   	
   	
   	}
   	}	
   	
   	for(String cast : splitCast3)
   	{
   	if(tokenName[i].toLowerCase().equals(cast.toLowerCase()))
   	{
   	cast3Mention=true;
   	int headId=headNodes[i];
   	String posTag=posNodes[i];
   	processedNodes[i]=1;
   	
   	if((posNodes[headId]).contains("VB"))
   	{
   	processedNodes[headId]=1;
   	List<Integer> referredNodes=findHeads(headId,headNodes);
   	Collections.sort(referredNodes);
   	double[] polarityscore = computePolarity(referredNodes,tokenName,posNodes);
   	double currentscore=posscore.get(cast3);
   	posscore.put(cast3,currentscore+polarityscore[0]);
   	currentscore=negscore.get(cast3);
   	negscore.put(cast3,currentscore+polarityscore[1]);
   	
   	}
   	
   	
   	}
   	}
   	
   	for(String cast : splitCast4)
   	{
   	if(tokenName[i].toLowerCase().equals(cast.toLowerCase()))
   	{
   	cast4Mention=true;
   	int headId=headNodes[i];
   	String posTag=posNodes[i];
   	processedNodes[i]=1;
   	
   	if((posNodes[headId]).contains("VB"))
   	{
   	processedNodes[headId]=1;
   	List<Integer> referredNodes=findHeads(headId,headNodes);
   	Collections.sort(referredNodes);
   	double[] polarityscore = computePolarity(referredNodes,tokenName,posNodes);
   	double currentscore=posscore.get(cast4);
   	posscore.put(cast4,currentscore+polarityscore[0]);
   	currentscore=negscore.get(cast4);
   	negscore.put(cast4,currentscore+polarityscore[1]);
   	
   	}
   	
   	
   	}
   	}
    }
    }
}

private double[] computePolarity(List<Integer> referredNodes, String[] tokenName, String[] posNodes) 
{
double pospolarityScore=0;
double negpolarityScore=0;
double prevpolarityScore=0;
String path="C:\\NLP\\Project\\models\\sentiwordnet.txt";
double[] score=new double[2];
int prevnode=-1;
try{
Sentiwordnet swordnet=new Sentiwordnet(path);
for(int node : referredNodes)
{
double nodepolairytScore = swordnet.extract(tokenName[node],(posNodes[node]=="NN"?"n":"a"));
if(prevpolarityScore>=0 && nodepolairytScore>=0)
{
if(prevnode==node-1)
pospolarityScore+=8*nodepolairytScore;
else
pospolarityScore+=7*nodepolairytScore;
}
if(prevpolarityScore<0 && nodepolairytScore>=0)
{
if(prevnode==node-1)
negpolarityScore+=8*nodepolairytScore;
else
pospolarityScore+=7*nodepolairytScore;
}
if(prevpolarityScore<0 && nodepolairytScore<0)
{
if(prevnode==node-1)
negpolarityScore+=8*nodepolairytScore;
else
negpolarityScore+=7*nodepolairytScore;
}
prevpolarityScore=nodepolairytScore;
prevnode=node;
}
score[0]=pospolarityScore;
score[1]=negpolarityScore;
}
catch(Exception e)
{
e.printStackTrace();
}
return score;
}

private List<Integer> findHeads(int headId, int[] headNodes) 
{
int count=0;
List<Integer> refNodes=new ArrayList<Integer>();
for(int i=1;i<headNodes.length;i++)
{
if(headNodes[i]==headId && processedNodes[i]!=1)
{
count++;
processedNodes[i]=1;
refNodes.add(i);
List<Integer> temprefNodes=findHeads(i,headNodes);
for(int temprefNode : temprefNodes)
refNodes.add(temprefNode);
}
}
return refNodes;
}

public List<String> process(AbstractTokenizer tokenizer, AbstractComponent[] components, String review)
     {
    AbstractSegmenter segmenter = EngineGetter.getSegmenter(language, tokenizer);
    InputStream is = new ByteArrayInputStream(review.getBytes());
         	BufferedReader br = new BufferedReader(new InputStreamReader(is));
         	DEPTree tree;
         	List<String> reviewlist=new ArrayList<String>();
         	for (List<String> tokens : segmenter.getSentences(br))
             {
            tree = NLPDecode.toDEPTree(tokens);
                     
                     for (AbstractComponent component : components)
                     {
                             component.process(tree);      
                     }
                     reviewlist.add(tree.toStringSRL());
             }
  
             return reviewlist;
     }
  
    private void ReadInputFile() {
    File file=new File("C:\\Users\\sumeshm\\workspace\\SemanticRoleLabeling\\src\\onereview.txt");
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
       	   String line;
       	   while ((line = br.readLine()) != null) {
       	   	
       	   	JSONParser jsonParser = new JSONParser();
       	   	JSONObject obj=(JSONObject)jsonParser.parse(line); 
       	   	String cast1=(String) obj.get("cast1");
       	   	String cast2=(String) obj.get("cast2");
       	   	String cast3=(String) obj.get("cast3");
       	   	String cast4=(String) obj.get("cast4");
       	   	String originalName=(String) obj.get("originalName");
       	   	JSONArray reviews=(JSONArray)obj.get("reviewsDetail");
       	   	for(int i=0;i<reviews.size();i++)
       	   	{
       	   	String review = (String) reviews.get(i);
       	   	}
       	   	
       	   	
       	   }
       	} catch (Exception e) {
e.printStackTrace();
}

}

 
final static String language = AbstractReader.LANG_EN;
public static void main(String[] args) {
String dictFile      = "C:\\NLP\\Project\\models\\dictionary-1.4.0.zip"; // e.g., dictionary.zip
         String posModelFile  = "C:\\NLP\\Project\\models\\ontonotes-en-pos-1.4.0.tgz"; // e.g., ontonotes-en-pos.tgz
         String depModelFile  = "C:\\NLP\\Project\\models\\ontonotes-en-dep-1.4.0.tgz"; // e.g., ontonotes-en-dep.tgz
         String predModelFile ="C:\\NLP\\Project\\models\\ontonotes-en-pred-1.4.0.tgz"; // e.g., ontonotes-en-pred.tgz
         String roleModelFile = "C:\\NLP\\Project\\models\\ontonotes-en-role-1.4.0.tgz"; // e.g., ontonotes-en-role.tgz
         String srlModelFile  = "C:\\NLP\\Project\\models\\ontonotes-en-srl-1.4.2.tgz"; // e.g., ontonotes-en-srl.tgz
         String inputFile     = "C:\\NLP\\Project\\models\\input.txt";
         String outputFile    = "C:\\NLP\\Project\\models\\output.txt";

         try
         {
                 new SemanticRoleLabel(dictFile, posModelFile, depModelFile, predModelFile, roleModelFile, srlModelFile, inputFile, outputFile);
        // print(dictFile, posModelFile, depModelFile, predModelFile, roleModelFile, srlModelFile, inputFile, outputFile);
         
         }
         catch (Exception e) {e.printStackTrace();}
Version.main(args);
}

}

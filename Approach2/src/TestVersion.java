import com.googlecode.clearnlp.run.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.util.List;

import com.googlecode.clearnlp.component.AbstractComponent;
import com.googlecode.clearnlp.dependency.DEPTree;
import com.googlecode.clearnlp.engine.EngineGetter;
import com.googlecode.clearnlp.nlp.NLPDecode;
import com.googlecode.clearnlp.nlp.NLPLib;
import com.googlecode.clearnlp.reader.AbstractReader;
import com.googlecode.clearnlp.segmentation.AbstractSegmenter;
import com.googlecode.clearnlp.tokenization.AbstractTokenizer;
import com.googlecode.clearnlp.util.UTInput;
import com.googlecode.clearnlp.util.UTOutput;

public class TestVersion {

	 public TestVersion(String dictFile, String posModelFile, String depModelFile, String predModelFile, String roleModelFile, String srlModelFile, String inputFile, String outputFile) throws Exception
     {
             AbstractTokenizer tokenizer  = EngineGetter.getTokenizer(language, new FileInputStream(dictFile));
             AbstractComponent tagger     = EngineGetter.getComponent(new FileInputStream(posModelFile) , language, NLPLib.MODE_POS);
             AbstractComponent analyzer   = EngineGetter.getComponent(new FileInputStream(dictFile)     , language, NLPLib.MODE_MORPH);
             AbstractComponent parser     = EngineGetter.getComponent(new FileInputStream(depModelFile) , language, NLPLib.MODE_DEP);
             AbstractComponent identifier = EngineGetter.getComponent(new FileInputStream(predModelFile), language, NLPLib.MODE_PRED);
             AbstractComponent classifier = EngineGetter.getComponent(new FileInputStream(roleModelFile), language, NLPLib.MODE_ROLE);
             AbstractComponent labeler    = EngineGetter.getComponent(new FileInputStream(srlModelFile) , language, NLPLib.MODE_SRL);
             
             AbstractComponent[] components = {tagger, analyzer, parser, identifier, classifier, labeler};
             
             String sentence = "it is a strong debut for kasdan , and shows great promise";
             process(tokenizer, components, sentence);
             process(tokenizer, components, UTInput.createBufferedFileReader(inputFile), UTOutput.createPrintBufferedFileStream(outputFile));
     }
     
     public void process(AbstractTokenizer tokenizer, AbstractComponent[] components, String sentence)
     {
             DEPTree tree = NLPDecode.toDEPTree(tokenizer.getTokens(sentence));
             
             for (AbstractComponent component : components)
                     component.process(tree);

             System.out.println(tree.toStringSRL()+"\n");
     }
     public void process(AbstractTokenizer tokenizer, AbstractComponent[] components, BufferedReader reader, PrintStream fout)
     {
             AbstractSegmenter segmenter = EngineGetter.getSegmenter(language, tokenizer);
             DEPTree tree;
             
             for (List<String> tokens : segmenter.getSentences(reader))
             {
                     tree = NLPDecode.toDEPTree(tokens);
                     
                     for (AbstractComponent component : components)
                             component.process(tree);
                     
                     fout.println(tree.toStringSRL()+"\n");
             }
             fout.close();
     }

    final String language = AbstractReader.LANG_EN;
	public static void main(String[] args) {
		 String dictFile      = "C:\\NLP\\Project\\models\\dictionary-1.4.0.zip"; // e.g., dictionary.zip
         String posModelFile  = "C:\\NLP\\Project\\models\\ontonotes-en-pos-1.4.0.tgz"; // e.g., ontonotes-en-pos.tgz
         String depModelFile  = "C:\\NLP\\Project\\models\\ontonotes-en-dep-1.4.0.tgz"; // e.g., ontonotes-en-dep.tgz
         String predModelFile ="C:\\NLP\\Project\\models\\ontonotes-en-pred-1.4.0.tgz"; // e.g., ontonotes-en-pred.tgz
         String roleModelFile = "C:\\NLP\\Project\\models\\ontonotes-en-role-1.4.0.tgz"; // e.g., ontonotes-en-role.tgz
         String srlModelFile  = "C:\\NLP\\Project\\models\\ontonotes-en-srl-1.4.2.tgz"; // e.g., ontonotes-en-srl.tgz
         String inputFile     = "C:\\NLP\\Project\\models\\input.txt";
         String outputFile    = "C:\\NLP\\Project\\models\\output.txt";

         try
         {
                 new TestVersion(dictFile, posModelFile, depModelFile, predModelFile, roleModelFile, srlModelFile, inputFile, outputFile);
         }
         catch (Exception e) {e.printStackTrace();}
		 Version.main(args);
	}

}
